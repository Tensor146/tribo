
HAS_DOCKER := $(shell command -v docker;)
HAS_DOCKER_COMPOSE := $(shell command -v docker-compose;)

ifndef HAS_DOCKER
	$(error no docker found)
endif
ifndef HAS_DOCKER_COMPOSE
	$(error no docker-compose found)
endif

install:
	go mod download

test:
	go test -v ./...

build:
	docker-compose -f build/package/docker-compose.yml build

up:
	docker-compose -f build/package/docker-compose.yml up -d

down:
	docker-compose -f build/package/docker-compose.yml down

.PHONY: install test build up down
