# Tribo

Tribo is a simple tribonacci numbers evaluation service, written on Golang.

## Features
  - Basic auth;
  - Redis as numbers storage (cache);
  - Embed event bus;
  - DI-based project structure;
  - Long arithmetics (math/big based);
  - See /api for api usage examples (via postman);
  - See /build/package for docker-compose file to run tribo easily;
  - Unit tests for business-logic (see /app/internal/tribo_evaluator);
  - API tests (embed in postman request, see /api for details);

## Build
  - Required tools: docker, docker-compose;
  - Evaluate "make up";
  - Check "127.0.0.1:3001/count/1";

## Alternate 
  - Required tools: docker, docker-compose;
  - Go to "/build/package";
  - Evaluate "docker-compose up -d";
  - Check "127.0.0.1:3001/count/1";

# Some notes about architecture
During development I tried to implement some "clean" DDD-style architecture, so I split application to several (application/domain/infrastructure) layers, which can be found in ./internal directory.
Such approach can look too complex for simple project (like this), but will be much more helpful in complex cases during professional development.

# Description
## Usage
   tribo [global options] command [command options] [arguments...]

## Version
   0.2.1

## Commands
     serve    Raises tribo daemon
     help, h  Shows a list of commands or help for one command

## Global options
   - --app-dir value           Application work directory (default: "%current") [$TRIBO_APP_DIR]
   - --log-level value         Logging level (default: "info") [$TRIBO_LOG_LEVEL]
   - --help, -h                show help
   - --version, -v             print the version

# Serve command
## Usage
   tribo serve [command options] [arguments...]

## Options
   - --http-host value         Http host address (default: "127.0.0.1") [$TRIBO_HTTP_HOST]
   - --http-port value         Http port (default: 3000) [$TRIBO_HTTP_PORT]
   - --http-username value     Http auth access check: username [$TRIBO_HTTP_USERNAME]
   - --http-password value     Http auth access check: password [$TRIBO_HTTP_PASSWORD]
   - --redis-host value        Redis host address (default: "127.0.0.1") [$TRIBO_REDIS_HOST]
   - --redis-port value        Redis port (default: 6379) [$TRIBO_REDIS_PORT]
   - --redis-password value    Redis password [$TRIBO_REDIS_PASSWORD]
   - --redis-db value          Redis database number (0-15) (default: 0) [$TRIBO_REDIS_DB]
