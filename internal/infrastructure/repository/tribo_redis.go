package repository

import (
	"fmt"
	"io"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/Tensor146/tribo/internal/config"
	"bitbucket.org/Tensor146/tribo/internal/domain/adapter"
	"bitbucket.org/Tensor146/tribo/internal/domain/service"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/helper"
	"github.com/bsm/redislock"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"

	"math/big"

	"github.com/sirupsen/logrus"
)

type TriboRedis struct {
	red redis.Cmdable
	log *logrus.Logger
	mtx sync.Mutex
}

func NewTriboRedis(cfg *config.Config, log *logrus.Logger) (*TriboRedis, error) {
	if err := helper.NewUnexistingDependencyError("config", cfg); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	ts := &TriboRedis{
		red: redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%d", cfg.RedisHost, cfg.RedisPort),
			Password: cfg.RedisPassword,
			DB:       int(cfg.RedisDB),
		}),
		log: log,
	}

	return ts, nil
}

func (ts *TriboRedis) Serve() error {
	_, err := ts.red.Ping().Result()
	return errors.Wrap(err, "can not connect to redis")
}

func (ts *TriboRedis) Stop() error {
	if redCloser, ok := ts.red.(io.Closer); ok {
		if err := redCloser.Close(); err != nil {
			return errors.Wrap(err, "can not close redis connection")
		}
	}

	return nil
}

func (ts *TriboRedis) Set(number int, value big.Int) error {
	ts.log.Debugf("Saving %d-%s", number, &value)
	return ts.set(ts.buildKey(ts.buildTriboNumberKey(number)), value.String())
}

func (ts *TriboRedis) set(number string, value string) error {
	if err := ts.red.Set(number, value, 0).Err(); err != nil {
		return errors.Wrapf(err, "can not set tribo: %s:%s", number, value)
	}
	return nil
}

func (ts *TriboRedis) MSet(pairs ...service.Pair) error {
	var pairs2 []interface{}
	isOdd := true
	for _, item := range pairs {
		if isOdd {
			pairs2 = append(pairs2, ts.buildTriboNumberKey(item.Number))
		} else {
			pairs2 = append(pairs2, item.Value)
		}
		isOdd = !isOdd
	}
	if err := ts.red.MSet(pairs2...).Err(); err != nil {
		return errors.Wrapf(err, "can not mset tribo: %v", pairs2)
	}
	return nil
}

func (ts *TriboRedis) Get(number int) (*big.Int, error) {
	ts.log.Debugf("Loading %d", number)
	rawRes, err := ts.red.Get(ts.buildKey(ts.buildTriboNumberKey(number))).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, adapter.ErrNotFound
		}
		return nil, errors.Wrapf(err, "can not get tribo %d", number)
	}
	res, ok := ts.parseTribo(rawRes)
	if !ok {
		return nil, errors.Wrapf(err, "can not parse tribo %d: %s", number, rawRes)
	}

	return res, nil
}

func (ts *TriboRedis) MGet(numbers ...int) ([]big.Int, error) {
	var keys []string

	for _, number := range numbers {
		keys = append(keys, ts.buildKey(ts.buildTriboNumberKey(number)))
	}
	rawRes, err := ts.red.MGet(keys...).Result()
	if err != nil {
		return nil, errors.Wrapf(err, "can not get tribo values (%v)", keys)
	}
	var values []big.Int
	for key, rawValue := range rawRes {
		if rawValue == nil {
			return nil, errors.Wrapf(err, "can not get tribo value: %d", numbers[key])
		}
		rawValString, ok := rawValue.(string)
		if !ok {
			return nil, errors.Wrapf(err, "invalid tribo value received for %d: %+v", numbers[key], rawValue)
		}
		value, ok := ts.parseTribo(rawValString)
		if !ok {
			return nil, errors.Wrapf(err, "can not parse tribo value %d: %s", numbers[key], rawValString)
		}
		values = append(values, *value)
	}

	return values, nil
}

func (ts *TriboRedis) GetMaxKnownNumber() (int, error) {
	ts.mtx.Lock()
	defer ts.mtx.Unlock()
	return ts.getMaxKnownNumber()
}

func (ts *TriboRedis) getMaxKnownNumber() (int, error) {
	ts.log.Debugf("Loading max known number")
	rawRes, err := ts.red.Get(ts.buildKey(keyMaxKnown)).Result()
	if err != nil {
		if err == redis.Nil {
			return 0, nil
		}
		return 0, errors.Wrap(err, "redis get error")
	}
	maxKnown, err := strconv.ParseInt(rawRes, 10, 0)
	if err != nil {
		return 0, errors.Wrapf(err, "can not parse value: %s", rawRes)
	}

	return int(maxKnown), nil
}

func (ts *TriboRedis) EnsureMaxKnownNumber(number int) error {
	ts.log.Debugf("Ensuring max known number %d", number)
	ts.mtx.Lock()
	defer ts.mtx.Unlock()
	locker := redislock.New(ts.red)
	lockKey := ts.buildKey("max_known_increment")
	lock, err := locker.Obtain(lockKey, 100*time.Millisecond, nil)
	if err != nil {
		return errors.Wrapf(err, "can not obtain lock %s", lockKey)
	}
	defer func() {
		if err := lock.Release(); err != nil {
			ts.log.Errorf("can not release lock %s", lockKey)
		}
	}()
	maxKnown, err := ts.getMaxKnownNumber()
	if err != nil {
		return err
	}
	if maxKnown > number {
		return nil
	}
	ts.log.Debugf("Saving max known number: %d", number)
	if err := ts.red.Set(ts.buildKey(keyMaxKnown), number, 0).Err(); err != nil {
		return errors.Wrapf(err, "can not set max known tribo: %d", number)
	}

	return nil
}

func (ts *TriboRedis) buildTriboNumberKey(number int) string {
	return ts.buildKey(fmt.Sprintf("number_%d", number))
}

func (ts *TriboRedis) buildKey(key string) string {
	return fmt.Sprintf("tribo:%s", key)
}

func (ts *TriboRedis) parseTribo(value string) (*big.Int, bool) {
	return new(big.Int).SetString(value, 10)
}
