package repository

import (
	"sync"

	"bitbucket.org/Tensor146/tribo/internal/domain/adapter"
	"bitbucket.org/Tensor146/tribo/internal/domain/service"

	"math/big"

	"github.com/pkg/errors"
)

const keyMaxKnown = "max_known"

type TriboInMemory struct {
	kv    sync.Map
	other sync.Map
	mtx   sync.Mutex
}

func NewTriboInMemory() *TriboInMemory {
	return &TriboInMemory{}
}

func (tr *TriboInMemory) Set(number int, value big.Int) error {
	tr.kv.Store(number, value)
	return nil
}

func (tr *TriboInMemory) Get(number int) (*big.Int, error) {
	res, found := tr.kv.Load(number)
	if !found {
		return nil, adapter.ErrNotFound
	}
	val, ok := res.(big.Int)
	if !ok {
		return nil, errors.Errorf("unknown value found: %+v", res)
	}

	return &val, nil
}

func (tr *TriboInMemory) MSet(pairs ...service.Pair) error {
	for _, pair := range pairs {
		tr.kv.Store(pair.Number, pair.Value)
	}

	return nil
}

func (tr *TriboInMemory) MGet(numbers ...int) ([]big.Int, error) {
	var results []big.Int
	for _, number := range numbers {
		res, err := tr.Get(number)
		if err != nil {
			return nil, err
		}
		results = append(results, *res)
	}
	return results, nil
}

func (tr *TriboInMemory) GetMaxKnownNumber() (int, error) {
	value, found := tr.other.Load(keyMaxKnown)
	if !found {
		return 0, nil
	}
	return value.(int), nil
}

func (tr *TriboInMemory) EnsureMaxKnownNumber(number int) error {
	tr.mtx.Lock()
	defer tr.mtx.Unlock()
	maxKnown, _ := tr.GetMaxKnownNumber()
	if maxKnown < number {
		tr.other.Store(keyMaxKnown, number)
	}
	return nil
}
