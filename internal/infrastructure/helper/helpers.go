package helper

import (
	"github.com/pkg/errors"
)

func NewUnexistingDependencyError(depName string, depValue interface{}) error {
	if depValue == nil {
		return errors.Errorf("unexisting dependency: %s", depName)
	}

	return nil
}
