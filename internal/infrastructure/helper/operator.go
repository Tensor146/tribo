package helper

import (
	"reflect"
	"sync"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Server interface {
	Serve() error
}

type Stopper interface {
	Stop() error
}

type Operator struct {
	mtx      sync.Mutex
	servers  []Server
	stoppers []Stopper
	log      *logrus.Logger
}

func NewOperator(log *logrus.Logger) (*Operator, error) {
	if err := NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	return &Operator{log: log}, nil
}

func (op *Operator) Serve() error {
	op.mtx.Lock()
	defer op.mtx.Unlock()
	for i, server := range op.servers {
		op.log.Debugf("Calling service #%d (%s)", i, reflect.TypeOf(server).String())
		if err := server.Serve(); err != nil {
			return errors.Wrapf(err, "can not call service #%d", i)
		}
	}
	return nil
}

func (op *Operator) Stop() error {
	op.mtx.Lock()
	defer op.mtx.Unlock()
	for i, stopper := range op.stoppers {
		op.log.Debugf("Calling stopper #%d (%s)", i, reflect.TypeOf(stopper).String())
		if err := stopper.Stop(); err != nil {
			return errors.Wrapf(err, "can not call stopper #%d", i)
		}
	}
	return nil
}

func (op *Operator) WithServers(servers ...Server) *Operator {
	return &Operator{
		mtx:      op.mtx,
		servers:  append(op.servers, servers...),
		stoppers: op.stoppers,
		log:      op.log,
	}
}

func (op *Operator) AddServers(servers ...Server) {
	op.mtx.Lock()
	defer op.mtx.Unlock()
	op.servers = append(op.servers, servers...)
}

func (op *Operator) WithStoppers(stoppers ...Stopper) *Operator {
	return &Operator{
		servers:  op.servers,
		mtx:      op.mtx,
		stoppers: append(op.stoppers, stoppers...),
		log:      op.log,
	}
}

func (op *Operator) AddStoppers(stoppers ...Stopper) {
	op.mtx.Lock()
	defer op.mtx.Unlock()
	op.stoppers = append(op.stoppers, stoppers...)
}
