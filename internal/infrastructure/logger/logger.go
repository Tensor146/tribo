package logger

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"bitbucket.org/Tensor146/tribo/internal/config"
)

const (
	LogLevelInfo = "info"
)

func NewLogger(cfg *config.Config) (*logrus.Logger, error) {
	log := logrus.New()
	logLevel, err := logrus.ParseLevel(cfg.LogLevel)
	if err != nil {
		return nil, errors.Wrapf(err, "can not parse log level \"%s\"", cfg.LogLevel)
	}
	log.SetLevel(logLevel)

	return log, nil
}
