package event_bus

import (
	"github.com/asaskevich/EventBus"
	"github.com/sirupsen/logrus"
)

func NewEventBus(log *logrus.Logger) EventBus.Bus {
	return EventBus.New()
}

func Close(bus EventBus.Bus) {
	bus.WaitAsync()
}
