package application

import (
	"math/big"

	"bitbucket.org/Tensor146/tribo/internal/domain/adapter"
	"bitbucket.org/Tensor146/tribo/internal/domain/event"
	"bitbucket.org/Tensor146/tribo/internal/domain/service"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/helper"
	"github.com/asaskevich/EventBus"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Tribonacci calculation service.
type Calculator struct {
	log         *logrus.Logger
	bus         EventBus.Bus
	tribosStore adapter.TriboNumbers
	eval        *service.Evaluator
}

func NewCalculator(
	log *logrus.Logger,
	bus EventBus.Bus,
	tribos adapter.TriboNumbers,
	eval *service.Evaluator,
) (*Calculator, error) {
	if err := helper.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("event_bus", bus); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("tribosStore", tribos); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("evaluator", eval); err != nil {
		return nil, err
	}

	return &Calculator{
		log:         log,
		bus:         bus,
		tribosStore: tribos,
		eval:        eval,
	}, nil
}

func (c *Calculator) Serve() error {
	if err := c.bus.SubscribeAsync(event.NewTribonacci, c.handleNewTribonacci, false); err != nil {
		return errors.Wrapf(err, "can not subscribe on event %s", event.NewTribonacci)
	}

	return nil
}

func (c *Calculator) handleNewTribonacci(eventObjRaw interface{}) {
	eventObj, ok := eventObjRaw.(event.EvNewTribonacci)
	if !ok {
		c.log.Warnf("received invalid object for %s event (%+v)", event.NewTribonacci, eventObjRaw)
	}
	c.log.Debugf("New tribonacci (%d-%s) was calculated! Saving...", eventObj.Number, &eventObj.Value)
	if err := c.tribosStore.Set(eventObj.Number, eventObj.Value); err != nil {
		c.log.Errorf("can not save tribonacci value %d-%s: %+v", eventObj.Number, &eventObj.Value, err)
	}
}

func (c *Calculator) Stop() error {
	if err := c.bus.Unsubscribe(event.NewTribonacci, c.handleNewTribonacci); err != nil {
		return errors.Wrapf(err, "can not unsubscribe on event %s", event.NewTribonacci)
	}
	return nil
}

func (c *Calculator) CalculateTribonacci(number int) (*big.Int, error) {
	if number < 0 {
		return nil, errors.Wrap(ErrInvalidArgument, "number should be >= 0")
	}
	result, ok := c.eval.GetSimple(number)
	if ok {
		return result, nil
	}

	maxKnown, err := c.tribosStore.GetMaxKnownNumber()
	c.log.Debugf("Max known number is %d", maxKnown)
	if err != nil {
		return nil, errors.Wrap(err, "can not get max known number")
	}
	if maxKnown >= number {
		found, err := c.tribosStore.Get(number)
		if err != nil {
			return nil, errors.Wrapf(err, "can not get tribo number %d", number)
		}
		return found, nil
	}
	if maxKnown == 0 {
		maxKnown = 2
	}

	initials := []service.Pair{
		*service.NewPair(maxKnown, *big.NewInt(0)),
		*service.NewPair(maxKnown-1, *big.NewInt(0)),
		*service.NewPair(maxKnown-2, *big.NewInt(0)),
	}

	for i, pair := range initials {
		var err error
		value, ok := c.eval.GetSimple(pair.Number)
		if !ok {
			if value, err = c.tribosStore.Get(pair.Number); err != nil {
				return nil, errors.Wrapf(err, "can not load initial value %d", pair.Number)
			}
		}
		initials[i].Value = *value
	}

	if result, err = c.eval.EvaluateTribonacci(number, initials[0], initials[1], initials[2]); err != nil {
		return nil, errors.Wrapf(err, "can not calculate number %d", number)
	}
	go func() {
		if err = c.tribosStore.EnsureMaxKnownNumber(number); err != nil {
			c.log.Errorf("Can not ensure max known number %d: %+v", number, err)
		}
	}()

	return result, nil
}
