package config

type Config struct {
	ServiceName   string
	AppDir        string
	LogLevel      string
	HttpHost      string
	HttpPort      uint
	HttpUser      string
	HttpPassword  string
	RedisHost     string
	RedisPort     uint
	RedisPassword string
	RedisDB       uint
}

func NewConfig() Config {
	return Config{}
}
