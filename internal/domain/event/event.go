package event

import "math/big"

const (
	NewTribonacci = "domain:new_tribonacci"
)

type EvNewTribonacci struct {
	Number int
	Value  big.Int
}

func NewEvNewTribonacci(number int, value big.Int) *EvNewTribonacci {
	return &EvNewTribonacci{Number: number, Value: value}
}
