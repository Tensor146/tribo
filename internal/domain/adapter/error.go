package adapter

import "github.com/pkg/errors"

var (
	ErrNotFound = errors.New("number not found")
)
