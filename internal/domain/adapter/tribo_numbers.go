package adapter

import (
	"math/big"

	"bitbucket.org/Tensor146/tribo/internal/domain/service"
)

type TriboNumbers interface {
	Get(number int) (*big.Int, error)
	Set(number int, value big.Int) error
	MSet(pairs ...service.Pair) error
	MGet(numbers ...int) ([]big.Int, error)
	GetMaxKnownNumber() (int, error)
	EnsureMaxKnownNumber(number int) error
}
