package service

import (
	"fmt"
	"math/big"
)

type Pair struct {
	Number int
	Value  big.Int
}

func (p Pair) String() string {
	return fmt.Sprintf("%d-%s", p.Number, &p.Value)
}

func NewPair(number int, value big.Int) *Pair {
	return &Pair{Number: number, Value: value}
}
