package service

import (
	"bitbucket.org/Tensor146/tribo/internal/domain/event"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/helper"
	"github.com/asaskevich/EventBus"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"math/big"
)

var (
	val0 = big.NewInt(0)
	val1 = big.NewInt(0)
	val2 = big.NewInt(1)
)

type Evaluator struct {
	log *logrus.Logger
	bus EventBus.Bus
}

func NewTriboEvaluator(log *logrus.Logger, bus EventBus.Bus) (*Evaluator, error) {
	if err := helper.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("event_bus", bus); err != nil {
		return nil, err
	}

	return &Evaluator{
		log: log,
		bus: bus,
	}, nil
}

// Caluculates N-th Tribonacci value using some initial M-th Tribonacci value (m < n) and also, M-1 and M-2.
func (ev *Evaluator) EvaluateTribonacci(
	n int,
	m Pair, // M
	m1 Pair, // M-1
	m2 Pair, // M-2
) (*big.Int, error) {
	if n < m.Number {
		return nil, errors.Errorf("invalid initial values, should be <n: %d", m.Number)
	}
	if m.Number-1 != m1.Number || m1.Number-1 != m2.Number {
		return nil, errors.Errorf("invalid initial values, should be m,m-1,m-2: %d %d %d",
			m.Number, m1.Number, m2.Number)
	}
	ev.log.Debugf("Calculating tribo: %d (known are %v-%v-%v)", n, m, m1, m2)
	initialIndex := m.Number
	t3 := &m.Value
	t2 := &m1.Value
	t1 := &m2.Value

	old := big.NewInt(0)
	ev.log.Debugf("Calculating %d: started with %s, %s, %s", n, t1, t2, t3)
	for i := initialIndex + 1; i <= n; i++ {
		old.Set(t3)
		t3.Add(t3, t2)
		t3.Add(t3, t1)
		ev.log.Debugf("Calculating %d: %s + %s + %s = %s", i, t1, t2, old, t3)
		t1.Set(t2)
		t2.Set(old)
		ev.bus.Publish(event.NewTribonacci, *event.NewEvNewTribonacci(i, *big.NewInt(0).Set(t3)))
	}

	return t3, nil
}

func (ev *Evaluator) GetSimple(number int) (*big.Int, bool) {
	switch number {
	case 0:
		return val0, true
	case 1:
		return val1, true
	case 2:
		return val2, true
	}

	return nil, false
}
