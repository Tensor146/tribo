package service

import (
	"testing"

	"bitbucket.org/Tensor146/tribo/internal/infrastructure/event_bus"

	"github.com/sirupsen/logrus"

	"math/big"
)

func createEvaluator(t *testing.T) *Evaluator {
	log := logrus.New()
	//log.SetLevel(logrus.DebugLevel)
	bus := event_bus.NewEventBus(log)
	ev, err := NewTriboEvaluator(log, bus)
	if err != nil {
		t.Fatalf("Error during evaluator construction: %s", err.Error())
	}

	return ev
}

func TestBase(t *testing.T) {
	ev := createEvaluator(t)
	fixtures := []struct {
		num int
		val string
	}{
		{0, "0"},
		{1, "0"},
		{2, "1"},
		{3, "1"},
		{4, "2"},
		{5, "4"},
		{6, "7"},
		{7, "13"},
		{8, "24"},
		{9, "44"},
		{10, "81"},
		{20, "35890"},
		{30, "15902591"},
		{37, "1132436852"},
	}
	var err error
	for _, fixture := range fixtures {
		val, ok := ev.GetSimple(fixture.num)
		if !ok {
			val, err = ev.EvaluateTribonacci(
				fixture.num,
				*NewPair(2, *big.NewInt(1)),
				*NewPair(1, *big.NewInt(0)),
				*NewPair(0, *big.NewInt(0)),
			)
			if err != nil {
				t.Errorf("Error during evaluation (%d): %s", fixture.num, err.Error())
				continue
			}
		}
		bigTestVal, ok := new(big.Int).SetString(fixture.val, 10)
		if !ok {
			t.Errorf("Error during evaluation: can not parse big int: %s", fixture.val)
			continue
		}
		if val.Cmp(bigTestVal) != 0 {
			t.Errorf("Invalid tribonacci number: tribo(%d) expected %s, received %s", fixture.num, fixture.val, val)
			continue
		}
		t.Logf("OK: trib(%d)=%s", fixture.num, val)
	}
}
