package http

import (
	"context"
	"net/http"
	"strconv"

	"bitbucket.org/Tensor146/tribo/internal/application"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/helper"

	"github.com/labstack/echo"
	logrusMiddleware "github.com/neko-neko/echo-logrus"
	logrusWrapper "github.com/neko-neko/echo-logrus/log"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"fmt"

	"github.com/labstack/echo/middleware"

	"bitbucket.org/Tensor146/tribo/internal/config"
)

type Server struct {
	e    *echo.Echo
	log  *logrus.Logger
	cfg  *config.Config
	calc *application.Calculator
}

func NewServer(calc *application.Calculator, log *logrus.Logger, cfg *config.Config) (*Server, error) {
	if err := helper.NewUnexistingDependencyError("calc", calc); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("log", log); err != nil {
		return nil, err
	}
	if err := helper.NewUnexistingDependencyError("cfg", cfg); err != nil {
		return nil, err
	}

	h := &Server{
		calc: calc,
		e:    echo.New(),
		log:  log,
		cfg:  cfg,
	}

	h.configureEcho(h.e)

	return h, nil
}

func (srv *Server) Serve() error {
	srv.log.Debugf("http: listening %s:%d", srv.cfg.HttpHost, srv.cfg.HttpPort)
	go func() {
		if err := srv.e.Start(fmt.Sprintf("%s:%d", srv.cfg.HttpHost, srv.cfg.HttpPort)); err != nil &&
			err != http.ErrServerClosed {
			srv.log.Fatal(errors.Wrap(err, "can not start http server"))
		}
	}()

	return nil
}

func (srv *Server) Stop() error {
	srv.log.Debugf("http: stopping")
	if err := srv.e.Shutdown(context.Background()); err != nil {
		return errors.Wrap(err, "can not stop http server")
	}

	return nil
}

func (srv *Server) configureEcho(e *echo.Echo) {
	e.Logger = &logrusWrapper.MyLogger{
		Logger: srv.log,
	}
	e.HideBanner = true
	e.HidePort = true
	// In future is better to replace it with non-global logger.
	e.Use(logrusMiddleware.Logger())
	if srv.cfg.HttpUser != "" || srv.cfg.HttpPassword != "" {
		e.Use(middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
			if username == srv.cfg.HttpUser && password == srv.cfg.HttpPassword {
				return true, nil
			}
			return false, nil
		}))
	}
	srv.registerRoutes(e)
}

func (srv *Server) registerRoutes(e *echo.Echo) {
	e.GET("/count/:number", func(ctx echo.Context) error {
		rawNumber := ctx.Param("number")
		if rawNumber == "" {
			return setErrResponse(400, "missing \"number\" param", ctx)
		}
		requestedNumber, err := strconv.ParseInt(rawNumber, 10, 0)
		if err != nil {
			return setErrResponse(400, fmt.Sprintf("can not parse number \"%s\"", rawNumber), ctx)
		}
		calculatedNumber, err := srv.calc.CalculateTribonacci(int(requestedNumber))
		if err != nil {
			if errors.Cause(err) == application.ErrInvalidArgument {
				return setErrResponse(400, err.Error(), ctx)
			}
			return errors.Wrapf(err, "can not get tribo number \"%d\"", requestedNumber)
		}
		err = ctx.JSON(200, map[string]interface{}{
			"requested_number": requestedNumber,
			"result":           calculatedNumber.String(),
		})
		if err != nil {
			return errors.Wrapf(err, "can not reply with json")
		}

		return nil
	})

	e.HTTPErrorHandler = func(e error, context echo.Context) {
		if he, ok := e.(*echo.HTTPError); ok {
			if he.Code >= 500 || he.Internal != nil {
				srv.e.DefaultHTTPErrorHandler(e, context)
			}
		} else {
			srv.e.DefaultHTTPErrorHandler(e, context)
		}
	}
}

func Stop(srv *Server) error {
	if err := srv.e.Shutdown(context.Background()); err != nil {
		return errors.Wrap(err, "error occurred during http server stop")
	}

	return nil
}

func setErrResponse(code int, errText string, ctx echo.Context) error {
	return ctx.JSON(code, map[string]interface{}{
		"message": errText,
	})
}
