package cli

import (
	"os"
	"os/signal"

	"bitbucket.org/Tensor146/tribo/internal/infrastructure/helper"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/logger"

	"bitbucket.org/Tensor146/tribo/internal/config"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Runs server application, working in daemonized mode.
func runServerApp(
	cfg *config.Config,
	construct func(cfg *config.Config, log *logrus.Logger, op *helper.Operator) error,
) error {
	log, err := logger.NewLogger(cfg)
	if err != nil {
		return errors.Wrap(err, "can not init logger")
	}
	op, err := helper.NewOperator(log)
	if err != nil {
		return errors.Wrap(err, "can not init operator")
	}

	if err = construct(cfg, log, op); err != nil {
		return errors.Wrap(err, "can not construct application")
	}

	log.Infof("App is starting with configuration: %+v", cfg)
	log.Debugf("Operator: %+v", op)

	if err := op.Serve(); err != nil {
		return errors.Wrap(err, "application serve error")
	}
	log.Info("App is serving")

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	log.Infof("App is stopping, reason: %s", <-c)

	return errors.Wrap(op.Stop(), "application stop error")
}
