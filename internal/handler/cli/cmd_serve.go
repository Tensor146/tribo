package cli

import (
	"bitbucket.org/Tensor146/tribo/internal/application"
	"bitbucket.org/Tensor146/tribo/internal/domain/service"
	http2 "bitbucket.org/Tensor146/tribo/internal/handler/http"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/event_bus"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/helper"
	"bitbucket.org/Tensor146/tribo/internal/infrastructure/repository"
	"github.com/pkg/errors"

	"bitbucket.org/Tensor146/tribo/internal/config"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

func cmdServe(cfg *config.Config) cli.Command {
	return cli.Command{
		Name:  "serve",
		Usage: "Raises tribo daemon",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:        "http-host",
				EnvVar:      "TRIBO_HTTP_HOST",
				Usage:       "Http host address",
				Value:       "127.0.0.1",
				Destination: &cfg.HttpHost,
			},
			cli.UintFlag{
				Name:        "http-port",
				EnvVar:      "TRIBO_HTTP_PORT",
				Usage:       "Http port",
				Value:       3000,
				Destination: &cfg.HttpPort,
			},
			cli.StringFlag{
				Name:        "http-username",
				EnvVar:      "TRIBO_HTTP_USERNAME",
				Usage:       "Http auth access check: username",
				Value:       "",
				Destination: &cfg.HttpUser,
			},
			cli.StringFlag{
				Name:        "http-password",
				EnvVar:      "TRIBO_HTTP_PASSWORD",
				Usage:       "Http auth access check: password",
				Value:       "",
				Destination: &cfg.HttpPassword,
			},
			cli.StringFlag{
				Name:        "redis-host",
				EnvVar:      "TRIBO_REDIS_HOST",
				Usage:       "Redis host address",
				Value:       "127.0.0.1",
				Destination: &cfg.RedisHost,
			},
			cli.UintFlag{
				Name:        "redis-port",
				EnvVar:      "TRIBO_REDIS_PORT",
				Usage:       "Redis port",
				Value:       6379,
				Destination: &cfg.RedisPort,
			},
			cli.StringFlag{
				Name:        "redis-password",
				EnvVar:      "TRIBO_REDIS_PASSWORD",
				Usage:       "Redis password",
				Value:       "",
				Destination: &cfg.RedisPassword,
			},
			cli.UintFlag{
				Name:        "redis-db",
				EnvVar:      "TRIBO_REDIS_DB",
				Usage:       "Redis database number (0-15)",
				Value:       0,
				Destination: &cfg.RedisDB,
			},
		},
		Action: func(_ *cli.Context) error {
			return runServerApp(cfg, func(cfg *config.Config, log *logrus.Logger, op *helper.Operator) error {
				tribos, err := repository.NewTriboRedis(cfg, log)
				if err != nil {
					return errors.Wrap(err, "can not init tribo redis repository")
				}
				bus := event_bus.NewEventBus(log)
				eval, err := service.NewTriboEvaluator(log, bus)
				if err != nil {
					return errors.Wrap(err, "can not init tribo evaluator")
				}
				calc, err := application.NewCalculator(log, bus, tribos, eval)
				if err != nil {
					return errors.Wrap(err, "can not init tribo calculator")
				}
				httpSrv, err := http2.NewServer(calc, log, cfg)
				if err != nil {
					return errors.Wrap(err, "can not init http server")
				}

				op.AddServers(tribos, calc, httpSrv)
				op.AddStoppers(httpSrv, calc, tribos)

				return nil
			})
		},
	}
}
