package cli

import (
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/Tensor146/tribo/internal/infrastructure/logger"

	"github.com/pkg/errors"
	"github.com/urfave/cli"

	"bitbucket.org/Tensor146/tribo/internal/config"
)

const ServiceName = "tribo"

const (
	flagAppDir = "app-dir"
)

const (
	flagValAppDirCurrent    = "%current"
	flagValAppDirWorkingDir = "%workdir"
)

func getVersion() string {
	return "0.2.1"
}

func Run() {
	cfg := config.NewConfig()
	cfg.ServiceName = ServiceName

	cliApp := cli.NewApp()
	cliApp.Name = "tribo"
	cliApp.Usage = "Tribonacci numbers evaluation service"
	cliApp.Version = getVersion()
	cliApp.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   flagAppDir,
			EnvVar: "TRIBO_APP_DIR",
			Usage:  "Application work directory",
			Value:  flagValAppDirCurrent,
		},
		cli.StringFlag{
			Name:        "log-level",
			EnvVar:      "TRIBO_LOG_LEVEL",
			Usage:       "Logging level",
			Value:       logger.LogLevelInfo,
			Destination: &cfg.LogLevel,
		},
	}

	cliApp.Before = func(context *cli.Context) error {
		var err error
		dir := context.String(flagAppDir)
		if dir == "" || dir == flagValAppDirCurrent {
			dir, err = filepath.Abs(filepath.Dir(os.Args[0]))
			if err != nil {
				return errors.Wrap(err, "can not get cliApp dir")
			}
		}
		if dir == flagValAppDirWorkingDir {
			dir, err = os.Getwd()
			if err != nil {
				return errors.Wrap(err, "can not get working dir")
			}
		}
		cfg.AppDir = dir

		return nil
	}

	cliApp.Commands = []cli.Command{
		cmdServe(&cfg),
	}

	err := cliApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
